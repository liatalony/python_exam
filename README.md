KEA BANK
Bank system project for Back-end with Python & Django elective course at KEA (Copenhagen School of Design and Technology)


Documentation:
https://liatalony.gitlab.io/python_exam/

Requirements (system features):
- A customer can have any number of bank accounts
- For future multi-factor authentication, we must record the customer’s telephone number
- The bank ranks its customers into three groups: basic, silver, and gold - the system must keep track of this information
- Customers ranked silver and gold can loan money from the bank
- Customers can make payments on their loans
- Customers can transfer money from their accounts if the account balance is sufficient
- Customers can view their accounts, accounts movements, and accounts balance
- Bank employees can view all customers and accounts
- Bank employees can create new customers and accounts and change customer rank
- Use Python 3.7 or newer, Django 3.0 or newer
- Customers must be able to transfer money to customers in at least one other bank in real-time (run two or more instances of the bank simultaneously)
- Implement some kind of multi-factor authentication for customers and bank employees
- Must implement at least one of these: REST, middleware, task queue, and channels
