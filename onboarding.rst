Onboarding
==========

Intro
*****
The entire technology stack of KEA's Bank consists of Python, Django, Nginx, PostgreSQL, Docker, Git, Highcharts.



Development tools
=================

Tmux
****
.. py:module:: tmux

Only have one terminal in use with vim, if sessions get stuck close them with:


Linting
*******
.. py:module:: watcher

It is recommended to watch the code compiling upon changes, keep one terminal reserved for watching the changes(watcher):

Pipeline
========

Stages
******
New stuff may be added but main stages must be respected (build, test, deploy)

Tests
*****
.. py:module:: pylama

- pylama tests and other tests, watcher running whats here?

.. py:module:: pytest

- to run unit tests

.. code-block::

    RTE=dev docker-compose run --rm --entrypoint "python3 manage.py test" app

Deploy pages
************
.. py:module:: documentation

Any changes to this documentation requires higher approval
Contents on ``onboarding.rst``, after editing the guidelines Sphinx needs to generate the html with the following commands:
.. code-block::

    make clean
    make html

Changes will be effective upon passing the pipeline AND merging in master (``gitlab yml`` file states rule master).
To view changes on your branch after commit comment out the master rule)

Docker
======
.. py:module:: docker

Docker-compose run development mode:

.. code-block::

   RTE=dev docker-compose up --build -d

Docker-compose run test in shell example:

.. code-block::

   RTE=dev docker-compose up
   docker-compose exec app sh
   coverage run --source="." manage.py test



Frameworks
==========
Third-party tools

Highcharts
**********
.. py:module:: highcharts

Script is available for the entire project from ``/templates/base.html``
For adding new graphs insert ``<script>`` directly on the desired html (if possible add to ``views.py`` instead)



API
===
.. py:module:: api

test
****
Something about API usage here
