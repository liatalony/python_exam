from rest_framework import generics
from .models import Account
from .serializers import AccountSerializer


class AccountList(generics.ListCreateAPIView):

    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def get_queryset(self):
        queryset = Account.objects.filter(user=self.request.user)
        return queryset


class AccountDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
