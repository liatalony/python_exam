from django.test import TestCase, Client
from .models import Rank
# from django.utils import timezone
from django.conf import settings


class AccountTestCase(TestCase):

    def setUp(self):
        pass


class SettingsTestCase(TestCase):
    # this test works
    def test_timezone_default(self):
        target_timezone = 'Europe/Copenhagen'
        self.assertEqual(target_timezone, settings.TIME_ZONE)
    print("Test: Correct timezone")

# class LoginViewsTestCase(TestCase):
    # def setUp(self):
    #    self.client = Client()
    # def test_index_redirects_login(self):
    #    response = self.client.get('', follow=True)
    #    response.redirect_chain
    #    self.assertEqual(response.status_code, 200)


class StaffViewsTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_staff_dashboard(self):
        pass


class RankModelTestCase(TestCase):

    def setUp(self):
        Rank.objects.create(customer='Kea', name='Gold', value='100')
        Rank.objects.create(customer='DTU', name='Silver', value='50')
        Rank.objects.create(customer='CBS', name='Basic', value='25')

    def test_gold_rank_value(self):
        gold_rank = Rank.objects.get(name='Gold')
        for rank in Rank.objects.all().exclude(name='Gold'):
            assert gold_rank.value > rank.value
        print("Test: Gold Rank is bigger than all")

    def test_silver_rank_value(self):
        gold_rank = Rank.objects.get(name='Gold')
        silver_rank = Rank.objects.get(name='Silver')
        basic_rank = Rank.objects.get(name='Basic')
        assert basic_rank.value <= silver_rank.value <= gold_rank.value
        print("Test: Silver Rank is between basic and gold")


class IndexViewTestCase(TestCase):

    def test_index_redirect(self):
        c = Client()
        response = c.get('/accounts/login/?next=/')
        self.assertTemplateUsed(response, 'registration/login.html')
        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'
        print("Test: Redirect login page")

    # def test_index_dashboard(self):
    #   c = Client()
    #   response = c.get('/staff_dashboard/', follow=True)
    #   print(response.redirect_chain)
    #   self.assertTemplateUsed(response.redirect_chain, 'staff_dashboard.html')


# class LedgerModelTestCase(TestCase):

    # def setUp(self):

    # def edge_case_transfer_completed_future(TestCase):
