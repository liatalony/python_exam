from rest_framework import serializers
from .models import Account


class AccountSerializer(serializers.ModelSerializer):
    """docstring serializer"""
    class Meta:
        """docstring serializer"""
        fields = ('id', 'user', 'name', 'balance', 'movements')
        model = Account
