upstream app_upstream {
    server app:8080;
}

server {
    listen 80;
    listen 443;
    ssl on;
    ssl_certificate /etc/letsencrypt/live/pbstyle.dk/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/pbstyle.dk/privkey.pem;

    server_name pbstyle.dk;

    location /static/ {
        alias /static/;
    }

    location /media/ {
        alias /media/;
    }

    location / {
        proxy_set_header Host $host;
        proxy_pass http://app_upstream;
    }
}
