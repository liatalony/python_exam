#!/bin/sh

echo "${RTE} Runtime Environment - Running entrypoint.!"

if [ "$RTE" = "dev" ]; then

    echo "This is development"
    python manage.py makemigrations --merge
    python manage.py migrate --noinput
    python manage.py createsuperuser --noinput --username $DJANGO_SUPERUSER_USERNAME --email $DJANGO_SUPERUSER_EMAIL
    echo "Add first use token"
    python manage.py addstatictoken $DJANGO_SUPERUSER_USERNAME
    echo "Run tests"
    python manage.py test
    python manage.py runserver 0:8000
    # python manage.py shell
    # python manage.py createsuperuser --noinput --username $DJANGO_SUPERUSER_USERNAME --email $DJANGO_SUPERUSER_EMAIL
    #echo "created super user"
	#python manage.py createsuperuser --noinput \
	 #   --username $DJANGO_SUPERUSER_USERNAME \
	 #   --email $DJANGO_SUPERUSER_USERNAME

elif [ "$RTE" = "test" ]; then

    echo "This is testing"
    python manage.py makemigrations
    python manage.py migrate
    echo "Run the tests:"
    python manage.py test
    python manage.py runserver

elif [ "$RTE" = "prod" ]; then

    echo "This is production"
    python manage.py check --deploy
    python manage.py collectstatic --noinput
    gunicorn kea_bank.asgi:application -b 0.0.0.0:8080 -k uvicorn.workers.UvicornWorker

fi
